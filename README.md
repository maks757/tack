1. docker-container up -d
2. run root_dir\dump.sql (host: localhost, db: rest, user: root, password: root)
3. Api licks <br>
    3.1. localhost/contacts/rest/mysql/add' <br>
    3.2. localhost/contacts/rest/elasticsearch/add' <br>
    3.3. localhost/contacts/rest/redis/add'
4. Use Bearer auth (token: as768df5at9wohi3o878f73ft7a7i)
4. Use request body <br>
    4.1 <br>
    { <br>
        "firstName": "Иван",<br>
        "lastName": "Иванов",<br>
        "phoneNumbers": [<br>
            "812 123-1234",<br>
            "916 123-4567"<br>
        ]<br>
    }<br>
    or<br>
    [<br>
    	{<br>
    		"firstName": "Иван",<br>
    		"lastName": "Иванов",<br>
    		"phoneNumbers": [<br>
    			"812 123-1234",<br>
    			"916 123-4567"<br>
    		]<br>
    	},<br>
    	{<br>
    		"firstName": "Иван",<br>
    		"lastName": "Иванов",<br>
    		"phoneNumbers": [<br>
    			"812 123-1234",<br>
    			"916 123-4567"<br>
    		]<br>
    	}<br>
    ]<br>

5. View link localhost/contacts<br>
    5.1. Change ContactsSearch model in root_dir\modules\controllers\ContactsController.php
6. Run worker localhost/contacts/contacts/run