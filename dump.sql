create table contacts
(
  id int auto_increment,
  firstName varchar(255) not null,
  lastName varchar(255) not null,
  phones json null,
  constraint persons_id_uindex
  unique (id)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

create index persons_firstName_lastName_index
  on contacts (firstName, lastName);

alter table contacts
  add primary key (id);