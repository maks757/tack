<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 09/09/2018
 * Time: 13:02
 */

namespace app\modules\contacts;


use yii\base\Module;

class ContactsModule extends Module
{
    public $defaultRoute = 'contacts';
}