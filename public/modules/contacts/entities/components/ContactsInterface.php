<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 09/09/2018
 * Time: 13:11
 */

namespace app\modules\contacts\entities\components;


interface ContactsInterface
{
    public static function transactionLoadAndSave($jsonData);
    public static function transactionSave($data);
}