<?php

namespace app\modules\contacts\entities\elasticsearch;

use app\modules\contacts\components\QueryJob;
use app\modules\contacts\entities\components\ContactsInterface;
use InvalidArgumentException;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property array $phones
 *
 */
class Contacts extends \yii\elasticsearch\ActiveRecord implements ContactsInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName'], 'required'],
            [['phones'], 'safe'],
            [['firstName', 'lastName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'phones' => 'Phones',
        ];
    }

//    /**
//     * {@inheritdoc}
//     * @return ContactsQuery the active query used by this AR class.
//     */
//    public static function find()
//    {
//        return new ContactsQuery([get_called_class()]);
//    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        // path mapping for '_id' is setup to field 'id'
        return ['id', 'firstName', 'lastName', 'phones'];
    }

    public function load($data, $formName = null)
    {
        $load_data = parent::load($data, $formName);
        if(isset($data['phoneNumbers'])) {
            $this->phones = Json::encode($data['phoneNumbers']);
        }
        return $load_data;
    }

    /**
     * @param $jsonData
     * @return bool|int|mixed
     * @throws \yii\db\Exception
     */
    public static function transactionLoadAndSave($jsonData)
    {
        if(!empty($jsonData)) {
            try {
                $request_data = Json::decode($jsonData);

                if(isset($request_data[0])){
                    foreach ($request_data as $data) {
                        Yii::$app->queue->push(new QueryJob([
                            'class_name' => '\app\modules\contacts\entities\elasticsearch\Contacts',
                            'load_data' => $data
                        ]));
                    }
                } else {
                    Yii::$app->queue->push(new QueryJob([
                        'class_name' => '\app\modules\contacts\entities\elasticsearch\Contacts',
                        'load_data' => $request_data
                    ]));
                }

                return 200;
            } catch(\Exception $e) {
                return 400;
            } catch(\Throwable $e) {
                return 400;
            }
        }
        return 400;
    }

    /**
     * @param $data
     * @return bool
     */
    public static function transactionSave($data)
    {
        $contact = new Contacts();
        $contact->load($data, '');
        if(!$contact->save()) {
            throw new InvalidArgumentException();
        }
        return true;
    }
}
