<?php

namespace app\modules\contacts\controllers;

use app\modules\contacts\components\mysql\ContactsSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContactsController implements the CRUD actions for Contacts model.
 */
class ContactsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contacts models.
     * @return mixed
     */
    public function actionIndex()
    {
        // elasticsearch
//        $searchModel = new \app\modules\contacts\components\elasticsearch\ContactsSearch();
        // mysql
//        $searchModel = new \app\modules\contacts\components\mysql\ContactsSearch();
        // redis
        $searchModel = new \app\modules\contacts\components\redis\ContactsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRun()
    {
        $queue = Yii::$app->queue;
        $queue->run(false, 1800);
    }
}
