<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 09/09/2018
 * Time: 14:15
 */

namespace app\modules\contacts\controllers\rest;

use app\modules\contacts\entities\elasticsearch\Contacts;
use yii\db\Exception;
use yii\filters\auth\HttpBearerAuth;

class ElasticSearchController extends \yii\rest\Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => 'yii\filters\auth\HttpBearerAuth',
        ];
        return $behaviors;
    }

    public function actionAdd()
    {
        // get data from body
        $entityBody = file_get_contents('php://input');

        // save data
        $response_code = Contacts::transactionLoadAndSave($entityBody);
        \Yii::$app->getResponse()->setStatusCode($response_code);
    }
}