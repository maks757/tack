<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 11/09/2018
 * Time: 14:06
 */

namespace app\modules\contacts\components;


use app\modules\contacts\entities\components\ContactsInterface;
use yii\base\BaseObject;
/**
 * @property ContactsInterface $class_name
 */
class QueryJob extends BaseObject implements \yii\queue\Job
{
    public $class_name;
    public $load_data;

    public function execute($queue)
    {
        $class = $this->class_name;
        $class::transactionSave($this->load_data);
    }
}